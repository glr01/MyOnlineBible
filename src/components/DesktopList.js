import React from "react"
import { ListGroup } from "react-bootstrap"

import mapBooksArray from "../utils/mapBooksArray"

export default function DesktopList({ books }) {
  // Checks if books are from old or new testament
  const lastIndex = books.length === 39 ? -19 : -7

  const firstBooks = books.slice(0, 20)
  const lastBooks = books.slice(lastIndex)

  return (
    <ListGroup horizontal variant="flush">
      <ListGroup.Item className="w-100">
        {firstBooks && mapBooksArray(firstBooks)}
      </ListGroup.Item>
      <ListGroup.Item className="w-100">
        {lastBooks && mapBooksArray(lastBooks)}
      </ListGroup.Item>
    </ListGroup>
  )
}
