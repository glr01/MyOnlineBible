import React, { useEffect, useState, useLayoutEffect } from "react"
import { ListGroup, Card } from "react-bootstrap"
import { useParams } from "react-router"
import { useHistory } from "react-router-dom"

import api from "../services/api"
import useViewport from "../hooks/useViewport"

export default function BookChapterSelect({ setCount }) {
  const book = []
  const history = useHistory()
  const { bookId } = useParams()
  const { innerWidth } = useViewport()
  const [bookName, setBookName] = useState("")
  const [chapterCount, setChapterCount] = useState(0)

  const mobile = innerWidth <= 500

  useLayoutEffect(() => {
    window.scrollTo(0, 0) // top
  })

  useEffect(() => {
    async function getChapterCount() {
      const response = await api.get(`/books/${bookId}`)
      setBookName(response.data.name)
      setChapterCount(response.data.chapters)
    }

    getChapterCount()
  }, [bookId])

  for (let i = 0; i < chapterCount; i++) {
    book.push(i + 1)
  }

  function handleClick(index) {
    setCount(chapterCount)
    history.push(`/nvi/book/${bookId}/${index + 1}`)
  }

  return (
    <>
      <style>
        {`
          .list-group {
            flex-direction: unset;
          }
        `}
      </style>

      <Card.Header
        className="py-4"
        style={{ color: "#0d6efd", fontSize: "1.5em", fontWeight: "bold" }}
      >
        {bookName}
      </Card.Header>
      <div className="d-flex" style={{ minHeight: "100vh" }}>
        <ListGroup className="flex-wrap w-100 rounded-0 text-center align-content-start">
          {book.map((chapter, index) => (
            <ListGroup.Item
              action
              key={index}
              onClick={() => handleClick(index)}
              style={{
                width: mobile ? "25%" : "20%",
                height: "5%",
                color: "#0d6efd",
                border: "1px solid rgba(0,0,0,.125)",
                fontSize: "1.2em"
              }}
            >
              {chapter}
            </ListGroup.Item>
          ))}
        </ListGroup>
      </div>
    </>
  )
}
