import React, { useState, useEffect } from "react"
import { Card } from "react-bootstrap"

import api from "../services/api"
import BooksList from "./BooksList"

export default function Dashboard() {
  const [otBooks, setOTBooks] = useState([])
  const [ntBooks, setNTBooks] = useState([])

  useEffect(() => {
    async function getBooks() {
      const response = await api.get("/books")
      setOTBooks(response.data.slice(0, 39))
      setNTBooks(response.data.slice(-27))
    }

    getBooks()
  }, [])

  return (
    <div className="w-100">
      <Card>
        <BooksList header="Antigo testamento" books={otBooks} />
      </Card>
      <Card>
        <BooksList header="Novo testamento" books={ntBooks} />
      </Card>
    </div>
  )
}
