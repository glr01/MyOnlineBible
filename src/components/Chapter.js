import React, { useEffect, useState, useLayoutEffect } from "react"
import { useParams, useHistory } from "react-router"
import { Navbar, Nav, Button, Placeholder } from "react-bootstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faChevronRight,
  faChevronLeft
} from "@fortawesome/free-solid-svg-icons"

import api from "../services/api"
import useViewport from "../hooks/useViewport"

const IconButton = props => (
  <Button
    {...props}
    style={{
      marginRight: "15px",
      background: "none",
      border: "none",
      transform: "scale(1.5)"
    }}
  >
    <FontAwesomeIcon icon={props.icon} />
  </Button>
)

export default function Chapter({ chapterCount }) {
  const history = useHistory()
  const { innerWidth } = useViewport()
  const [name, setName] = useState("")
  const [verses, setVerses] = useState([])
  const { bookId, chapterIndex } = useParams()

  const underThreshold = innerWidth <= 500

  const parsedChapterIndex = parseInt(chapterIndex)

  useLayoutEffect(() => {
    window.scrollTo(0, 0)
  }, [chapterIndex])

  useEffect(() => {
    async function getVerses() {
      const response = await api.get(`/verses/nvi/${bookId}/${chapterIndex}`)

      setName(response.data.book.name)
      setVerses(response.data.verses)
    }

    getVerses()
  }, [bookId, chapterIndex])

  function handleClick(index) {
    history.push(`/nvi/book/${bookId}/${index}`)
  }

  return (
    <>
      {!name && (
        <Placeholder animation="glow">
          <Placeholder
            className="w-100"
            style={{ height: "80px" }}
            bg="primary"
          />
        </Placeholder>
      )}

      {name && (
        <>
          <Navbar
            sticky="top"
            variant="dark"
            className="d-flex"
            style={{
              backgroundColor: "#2c5fab",
              borderBottom: "1px solid rgba(0,0,0,.125)"
            }}
          >
            <Navbar.Brand
              className="py-2 mx-3 flex-grow-1"
              style={{ fontSize: "1.8em", fontWeight: "bold" }}
            >
              {name + " " + chapterIndex}
            </Navbar.Brand>
            <Nav style={{ fontSize: underThreshold ? 17 : 19 }}>
              <Nav.Link onClick={() => history.push("/")}>Home</Nav.Link>
              <Nav.Link onClick={() => history.push(`/book/${bookId}`)}>
                Capítulos
              </Nav.Link>
              <Nav.Link
                as={IconButton}
                icon={faChevronLeft}
                disabled={parsedChapterIndex === 1}
                onClick={() => handleClick(parsedChapterIndex - 1)}
              />
              <Nav.Link
                as={IconButton}
                icon={faChevronRight}
                disabled={parsedChapterIndex === chapterCount}
                onClick={() => handleClick(parsedChapterIndex + 1)}
              />
            </Nav>
          </Navbar>

          {verses.map(verse => (
            <div key={verse.number} className="m-3 d-flex flex-row">
              <h5
                className="text-muted user-select-none"
                style={{ marginRight: "5px" }}
              >
                {verse.number}
              </h5>
              <h3>{verse.text}</h3>
            </div>
          ))}
        </>
      )}
    </>
  )
}
