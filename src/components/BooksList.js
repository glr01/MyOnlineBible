import React from "react"
import { Card } from "react-bootstrap"

import DesktopList from "./DesktopList"
import useViewport from "../hooks/useViewport"
import mapBooksArray from "../utils/mapBooksArray"

export default function BooksList({ header, books }) {
  const { innerWidth } = useViewport()

  return (
    <>
      <Card.Header
        className="py-4"
        style={{ color: "#0d6efd", fontSize: "1.5em", fontWeight: "bold" }}
      >
        {header}
      </Card.Header>
      <Card.Body>
        {innerWidth <= 500 ? (
          mapBooksArray(books)
        ) : (
          <DesktopList books={books} />
        )}
      </Card.Body>
    </>
  )
}
