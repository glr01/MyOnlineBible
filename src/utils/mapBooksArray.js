import { ListGroup } from "react-bootstrap"
import { Link } from "react-router-dom"

export default function mapBooksArray(booksArray) {
  return booksArray.map(book => (
    <ListGroup.Item
      key={book.abbrev.pt}
      variant="secondary"
      as={Link}
      to={`/book/${book.abbrev.pt}`}
      style={{ fontSize: "1.3em" }}
      action
    >
      {book.name}
    </ListGroup.Item>
  ))
}
