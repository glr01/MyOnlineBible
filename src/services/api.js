import axios from "axios"

const api = axios.create({
  baseURL: "https://www.abibliadigital.com.br/api",
  headers: {
    Authorization: `Bearer ${process.env.REACT_APP_API_TOKEN}`
  }
})

export default api
