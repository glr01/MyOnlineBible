import React, { useState } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Dashboard from "./components/Dashboard"
import BookChapterSelect from "./components/BookChapterSelect"
import Chapter from "./components/Chapter"

function App() {
  const [chapterCount, setChapterCount] = useState()

  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route
          exact
          path="/book/:bookId"
          component={() => <BookChapterSelect setCount={setChapterCount} />}
        />
        <Route
          path="/nvi/book/:bookId/:chapterIndex"
          component={() => <Chapter chapterCount={chapterCount} />}
        />
      </Switch>
    </Router>
  )
}

export default App
